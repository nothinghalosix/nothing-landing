import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";

export default defineConfig({
  site: 'https://nothinghalosix.gitlab.io',
  //base: '/nothing-landing',
  outDir: 'public',
  publicDir: 'static',
  integrations: [tailwind()],
});